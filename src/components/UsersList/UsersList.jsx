import React, { Component } from 'react'
import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import {getUsersData, saveUsersData} from '../../utils/storage';
import EditUser from '../../components/EditUser/EditUser'
import './UsersList.css';
import {formatBirthdayToString} from "../../utils/formatDate";
import {EMPTY_USER} from '../../constants/defaultUserList'

class UsersList extends Component {
    constructor(props) {
        super(props);

        const users = getUsersData();

        this.state = {
            users,
            editUser: null,
            editUserId: null
        };

        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.onSaveEdit = this.onSaveEdit.bind(this);
        this.onCancelEdit = this.onCancelEdit.bind(this);
        this.onDeleteItem = this.onDeleteItem.bind(this);
    }

    componentDidUpdate(prevProps, prevState) {
        if(prevState.user && prevState.user.phone !== this.state.user.phone) {
            console.log('Changed!', this.state.user.phone);
        }
    }

    closeModal() {
        this.setState({
            editUser: null,
            editUserId: null
        })
    }

    openModal(editUser, editUserId) {
        this.setState({
            editUser,
            editUserId
        });
    }

    onSaveEdit(user, id) {
        let {users} = this.state;
        if(id === null) {
            users.push(user);
        } else {
            users = users.map((item, index) => {
                if(index !== id) {
                    return item;
                }
                return {
                    ...user
                };
            });
        }

        this.setState({
            users
        });

        saveUsersData(users);

        this.closeModal();
    }

    onDeleteItem(id) {
        let {users} = this.state;
        users = users.filter((item, index) => {
            return index !== id;
        });
        this.setState({
            users
        });

        saveUsersData(users);
    }

    onCancelEdit() {
        this.closeModal();
    }

    render() {
        const {users, editUser, editUserId} = this.state;

        return (
            <div id="usersListWrapper">
                <div className={'header'}>
                    <Button variant="contained" color="primary"
                            onClick ={() => {
                                this.openModal(EMPTY_USER, null);
                            }}>
                        Добавить пользователя
                    </Button>
                </div>

                {editUser &&
                    <EditUser
                        user={editUser}
                        id={editUserId}
                        onSaveEdit={this.onSaveEdit}
                        onCancelEdit={this.onCancelEdit}
                    />
                }

                <Table className={''} style={{width: '100%'}}>
                    <TableHead>
                        <TableRow>
                            <TableCell>ФИО</TableCell>
                            <TableCell>Дата рождения</TableCell>
                            <TableCell>Адрес</TableCell>
                            <TableCell>Город</TableCell>
                            <TableCell>Телефон</TableCell>
                            <TableCell>Действие</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {users.map((user, i) => {
                            return (<TableRow key={i}>
                                <TableCell component="th" scope="row">
                                    {user.userName}
                                </TableCell>
                                <TableCell>{formatBirthdayToString(user.dateOfBirth)}</TableCell>
                                <TableCell>{user.address}</TableCell>
                                <TableCell>{user.location}</TableCell>
                                <TableCell>{user.phone}</TableCell>
                                <TableCell>
                                    <div className={'actionIcon edit'}
                                         onClick ={() => {
                                             this.openModal(user, i);
                                         }}
                                    ></div>
                                    <div className={'actionIcon delete'}
                                        onClick = {() => {
                                            this.onDeleteItem(i);
                                        }}
                                    >

                                    </div>
                                </TableCell>
                            </TableRow>)
                        })}
                    </TableBody>
                </Table>
            </div>
        )
    }
}

export  default UsersList