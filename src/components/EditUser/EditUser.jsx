import React, { Component } from 'react'
import {formatMonthToString} from "../../utils/formatDate";

import Button from '@material-ui/core/Button';
import './EditUser.css';

const days = Array.from(Array(31).keys()).map(n => n + 1);
const months = Array.from(Array(12).keys()).map(n => n + 1);
const year = new Date().getFullYear();
const years = Array.from({length: year - 1900}, (value, index) => 1901 + index);

class EditUser extends Component {
    constructor(props) {
        super(props);

        const {user, id} = props;

        this.state = {
            user,
            id,
            errors: this.checkForm(user)
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.checkForm = this.checkForm.bind(this);
    }

    handleInputChange(event) {
        let {user} = this.state;
        let nextUser = {};

        let splitName = event.target.name.split('.');
        if(splitName.length > 1) {
            nextUser = {
                ...user,
                [splitName[0]]: {
                    ...user[splitName[0]],
                    [splitName[1]]: parseInt(event.target.value)
                }
            }
        } else {
            nextUser = {
                ...user,
                [event.target.name]: event.target.value
            }

        }

        const errors = this.checkForm(nextUser);

        this.setState({
            user: nextUser,
            errors
        });
    };

    checkForm(user) {
        let errors = [];

        function isValidPhoneNumber(p) {
            let regex = RegExp('^((\\+7|7|8)+([0-9]){10})$','gm');
            return regex.test(p);
        }

        if(user.userName.length === 0) {
            errors.push({key: 'userName', text: 'Поле ФИО должно быть заполнено'});
        } else if(user.userName.length > 100) {
            errors.push({key: 'userName', text: 'Поле ФИО не должно превышать 100 символов'});
        } else if(!(isValidPhoneNumber(user.phone))) {
            errors.push({key: 'phone', text: 'Неверный формат номера телефона'});
        }

        return errors;
    }



    render() {
        const {user, id, errors} = this.state;
        const {onSaveEdit, onCancelEdit} = this.props;
        return (
            <div className={'editUserWrapper'}>

                {id !== null &&
                    <div>Изменение данных пользователя</div>
                }

                {id === null &&
                    <div>Добавление нового пользователя</div>
                }

                <br/>

                <div className={'editUserBody'}>
                    <form>
                        ФИО: <input type="text" name="userName" value={user.userName} onChange={this.handleInputChange}/>
                        <br/>
                        Дата рождения:
                        <select name={'dateOfBirth.day'} defaultValue={user.dateOfBirth.day} onChange={this.handleInputChange}>
                            {days.map((item) =>
                                <option value = {item} key={item}>
                                    {item}
                                </option>
                            )}
                            );
                        </select>
                        <select name={'dateOfBirth.month'} defaultValue={user.dateOfBirth.month} onChange={this.handleInputChange}>
                            {months.map((item) =>
                                <option value = {item} key={item}>
                                    {formatMonthToString(item)}
                                </option>
                            )}
                            );
                        </select>
                        <select name={'dateOfBirth.year'} defaultValue={user.dateOfBirth.year} onChange={this.handleInputChange}>
                            {years.map((item) =>
                                <option value = {item} key={item}>
                                    {item}
                                </option>
                            )}
                            );
                        </select>
                        <br/>
                        Адрес: <input type="text" name="address" value={user.address} onChange={this.handleInputChange}/><br/>
                        Город: <input type="text" name="location" value={user.location} onChange={this.handleInputChange}/><br/>
                        Телефон: <input type="text" name="phone" value={user.phone} onChange={this.handleInputChange}/>
                        <br/>

                    </form>
                </div>

                <div className={'errorList'}>
                    {errors.map(err => {
                        return <div className={'errorText'} key={err.text}>{err.text}</div>
                    })}
                </div>

                <div className={'editUserButtons'}>
                    <Button variant="contained" color="primary"
                            disabled={errors.length > 0}
                            onClick ={() => {
                                if(errors.length === 0){
                                    onSaveEdit(user, id);
                                }
                            }}>
                        Сохранить
                    </Button>
                    <Button variant="contained"
                            onClick ={onCancelEdit} style={{'marginLeft':'10px'}}>
                        Отмена
                    </Button>
                </div>


            </div>
        )
    }
}

export  default EditUser