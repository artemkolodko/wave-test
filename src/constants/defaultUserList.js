export const EMPTY_USER = {
    userName: '',
    dateOfBirth: {
        day: 1,
        month: 1,
        year: 1990
    },
    address: '',
    location: '',
    phone: ''
};

export const DEFAULT_USERS_LIST = [{
        userName: 'Фейнман Ричард Филлипс',
        dateOfBirth: {
            day: 1,
            month: 5,
            year: 1918
        },
        address: '7100, 16 Авеню',
        location: 'Нью-Йорк',
        phone: '+79817034405'
    },
    {
        userName: 'Сахаров Андрей Дмитриевич',
        dateOfBirth: {
            day: 21,
            month: 5,
            year: 1921
        },
        address: 'Маросейка, 15',
        location: 'Москва',
        phone: '+79293101215'
    }];