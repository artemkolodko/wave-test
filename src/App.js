import React, { Component } from 'react';


import UsersList from './components/UsersList/UsersList';
import './App.css';

class App extends Component {

    render() {
        return (
        <div className="App">
          <header className="App-header">
              <UsersList/>
          </header>
        </div>
        );
    }
}

export default App;
