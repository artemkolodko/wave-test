import {DEFAULT_USERS_LIST} from '../constants/defaultUserList'

const STORAGE_KEY = 'appUsersList';

export const saveUsersData = (data) => {
    localStorage.setItem(STORAGE_KEY, JSON.stringify(data));
};

export const getUsersData = () => {
    const usersList = localStorage.getItem(STORAGE_KEY);
    if (usersList === null) {
        return DEFAULT_USERS_LIST;
    }

    return JSON.parse(usersList);
};