const monthsTable = {
    1: 'января',
    2: 'февраля',
    3: 'марта',
    4: 'апреля',
    5: 'мая',
    6: 'июня',
    7: 'июля',
    8: 'августа',
    9: 'сентября',
    10: 'октября',
    11: 'ноября',
    12: 'декабря'
};

export const formatBirthdayToString = (dateOfBirth) => {
    const {day, month, year} = dateOfBirth;
    const monthAlias = monthsTable[month];
  return `${day} ${monthAlias} ${year}`
};

export const formatMonthToString = (index) => {
    return monthsTable[index];
}